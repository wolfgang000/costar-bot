import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import TimeoutException
import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DRIVER_PATH = os.path.join(BASE_DIR, 'geckodriver/')
AUTH_FILE_PATH = os.path.join(BASE_DIR, 'AUTH.INI')
print(BASE_DIR)
print(DRIVER_PATH)

class LoginRunner:
	
	LOGIN_URL = "https://gateway.costar.com/login"

	def __init__(self, username, password):
		self.username = username
		self.password = password
		self.driver = webdriver.Firefox()
		self.driver.get(self.LOGIN_URL)
	

	def is_code_field_present(self):
		try:
			input_code = WebDriverWait(self.driver, 20).until(
				expected_conditions.visibility_of_element_located((By.ID, "code"))
			)
			return True
		except Exception:
			return False

	def send_code(self, code):
		input_code = WebDriverWait(self.driver, 20).until(
			expected_conditions.visibility_of_element_located((By.ID, "code"))
		)
		input_code.send_keys(code)
		submit_button = WebDriverWait(self.driver, 20).until(
			expected_conditions.visibility_of_element_located((By.ID, "otpverify"))
		)
		submit_button.click()

		

	def login(self):
		username_field = self.driver.find_element_by_id("username")
		password_field = self.driver.find_element_by_id("password")
		remember_me_field = self.driver.find_element_by_xpath("//label[@for='rememberMe']")

		username_field.send_keys(self.username)
		password_field.send_keys(self.password)
		
		if not remember_me_field.is_selected():
			remember_me_field.click()

		submit_button = WebDriverWait(self.driver, 20).until(
			expected_conditions.visibility_of_element_located((By.ID, "loginButton"))
		)
		
		submit_button.click()

		error_msg = "Invalid username/password combination"
	
	def quit(self):
		self.driver.quit()

	def get_cookies(self):
		cookie_dict = {}
		for cookie in self.driver.get_cookies():
			cookie_name = cookie['name']
			cookie_value = cookie['value']
			cookie_dict[cookie_name] = cookie_value
		return cookie_dict

import json



def save_dict_to_file(dictionary, path):
	with open(path, 'w') as file:
		file.write(json.dumps(dictionary))

import configparser


def main():
	config = configparser.ConfigParser()
	config.read(AUTH_FILE_PATH)
	default = config['DEFAULT']
	username = default['USERNAME']
	password = default['PASSWORD']

	runner = LoginRunner(username,password)
	runner.login()
	if runner.is_code_field_present():
		code = input("Write the autoritation code: ")
		runner.send_code(code)
	time.sleep(30)
	
	save_dict_to_file(runner.get_cookies(), os.path.join(BASE_DIR, 'cookies.json'))
	runner.quit()
	

if __name__ == '__main__':
    main()
