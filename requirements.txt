certifi==2018.1.18
chardet==3.0.4
idna==2.6
lxml==4.1.1
marshmallow==2.15.0
requests==2.18.4
selenium==3.11.0
urllib3==1.22
