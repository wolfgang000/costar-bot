# Instructions

## Credentials: Username and Password

Put your username and password in the file AUTH.INI, if this file doesn't exist copy the example file AUTH.INI.example and rename it as AUTH.INI

EXAMPLE: the content should look like this:

```
[DEFAULT]
USERNAME=my_username
PASSWORD=my_password
```

## Open the BOT

There are two ways of running the BOT:

1) Access in your desktop the Direct Access with the name: COSTAR-BOT

2) Access the project folder and execute the command:

```
python3 costar_bot.py
```


## Set up the search terms

In the BOT interface setup the search parameters in the three tabs.

Be aware that this version only search for a top 500 results. So the more specific the better.


## Result Files

The search results are store in the "files" folder as csv files, this files contains names and emails of the true buyer, the buyer and the seller.

You can access the "files" folder in two ways:

1) Access in your Desktop the Direct Access with the name: FILES

2) Access the project folder and open the 'files' folder


