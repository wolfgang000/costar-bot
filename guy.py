import tkinter as tk
from tkinter import messagebox, Frame, Checkbutton
from tkinter import ttk
import configparser
import os
import json
from costar_bot import main

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


"""
	USERNAME=mm_rissenberg
	PASSWORD=holland9
	REQUEST_DELAY=1
	PROPERTY_TYPES=flex,office
	PROPERTY_SUB_TYPES=

	TENANCIES=single
	STATES=Alaska,Alabama,Arizona
	
	config = configparser.ConfigParser()
	config['DEFAULT'] = {'ServerAliveInterval': '45',
						'Compression': 'yes',
						'CompressionLevel': '9'}
	with open('example.ini', 'w') as configfile:
		config.write(configfile)
	print("hi there, everyone!")
"""
def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

from tkinter import Tk, Label, Button, Entry, IntVar, END, W, E, N, S


class Checkbar(Frame):
	def __init__(self, parent=None, picks=[], side=tk.LEFT, anchor=W):
		Frame.__init__(self, parent)
		self.vars = []
		self.picks = picks
		for pick in picks:
			var = IntVar()
			chk = Checkbutton(self, text=pick, variable=var)
			chk.pack(side=side, anchor=anchor, expand=tk.YES)
			self.vars.append(var)
	def state(self):
		return map((lambda var: var.get()), self.vars)
	def checked_names(self):
		names = []
		indices = list(self.state())
		for is_checked, name in zip(indices, self.picks):
			if is_checked == 1:
				names.append(name)
		return names
		

class CostarGui:

	def __init__(self, master):
		self.master = master
		master.title("Costar")

		self.nb = ttk.Notebook(root)

		self.page1 = ttk.Frame(self.nb) 
		self.page2 = ttk.Frame(self.nb)
		self.page3 = ttk.Frame(self.nb)
		self.nb.add(self.page1, text='Main')
		self.nb.add(self.page2, text='States')
		self.nb.add(self.page3, text='Secondary Type')

		self.nb.pack(expand=1, fill="both")

		self.min_price = 0
		self.max_price = 0
		self.sale_date_start = ""
		self.sale_date_end = ""
		##
		self.max_price_label = Label(self.page1, text="Min Price:")
		v_max_price = self.page1.register(self.validate_min_price) # we have to wrap the command
		self.max_price_entry = Entry(self.page1, validate="key", validatecommand=(v_max_price, '%P'))

		self.max_price_label.grid(row=1, column=0,  sticky=W)
		self.max_price_entry.grid(row=1, column=1,  sticky=E)
		###
		
		###
		self.min_price_label = Label(self.page1, text="Max Price:")
		v_min_price = self.page1.register(self.validate_max_price) # we have to wrap the command
		self.min_price_entry = Entry(self.page1, validate="key", validatecommand=(v_min_price, '%P'))

		self.min_price_label.grid(row=2, column=0, sticky=W)
		self.min_price_entry.grid(row=2, column=1, sticky=E)
		###

		self.non_disclosed_price_checkbox = Checkbar(self.page1, ['include-non-disclosed-price'])
		self.non_disclosed_price_checkbox.grid(row=2, column=3, sticky=E)

		###
		self.sale_date_start_label = Label(self.page1, text="Sale Start Date(MM/DD/YYYY):")
		v_sale_date_start = self.page1.register(self.validate_sale_date_start) # we have to wrap the command
		self.sale_date_start_entry = Entry(self.page1, validate="key", validatecommand=(v_sale_date_start, '%P'))

		self.sale_date_start_label.grid(row=3, column=0, sticky=W)
		self.sale_date_start_entry.grid(row=3, column=1, sticky=E)
		###


		###
		self.sale_date_end_label = Label(self.page1, text="Sale End Date(MM/DD/YYYY):")
		v_sale_date_end = self.page1.register(self.validate_sale_date_end) # we have to wrap the command
		self.sale_date_end_entry = Entry(self.page1, validate="key", validatecommand=(v_sale_date_end, '%P'))

		self.sale_date_end_label.grid(row=4, column=0, sticky=W)
		self.sale_date_end_entry.grid(row=4, column=1, sticky=E)

		###

		self.tenancies = Checkbar(self.page1, ['single', 'multiple'])
		self.tenancies_label = Label(self.page1, text="Tenancies:")
		self.tenancies_label.grid(row=5, column=0, sticky=W)
		self.tenancies.grid(row=5, column=1, sticky=E)

		property_types_names = [
			'hospitality','industrial','land','office','retail',
			'flex','sports-entertainment','specialty',
			'multi-family','health-care','shopping-center',
		]
		
		self.property_types = Checkbar(self.page1, property_types_names,side=tk.TOP)
		self.property_types_label = Label(self.page1, text="Property Types:")
		self.property_types_label.grid(row=6, column=0, sticky=W)
		self.property_types.grid(row=7, column=0, sticky=E)

		sale_status_names = [
			'sold','for-sale','under-contract'
		]

		self.sale_status = Checkbar(self.page1, sale_status_names,side=tk.TOP)
		self.sale_status_label = Label(self.page1, text="Sale status:")
		self.sale_status_label.grid(row=6, column=2, sticky=W)
		self.sale_status.grid(row=7, column=2, sticky=E)

		other_options_names = [
			'include-public-record-sales',
			'include-non-arms-length-comps',
			'include-nnn-sales-only',
			'return-portfolios-as-individuals',
		]

		self.other_options = Checkbar(self.page1, other_options_names,side=tk.TOP)
		self.other_options_label = Label(self.page1, text="Other Options:")
		self.other_options_label.grid(row=6, column=3, sticky=W)
		self.other_options.grid(row=7, column=3, sticky=E)



		#######
		# Page 2

		states_path = os.path.join(BASE_DIR, os.path.join('resources','states.json'))
		with open(states_path, 'r') as file:
			states_json = file.read()
		self.STATES_MAP = json.loads(states_json)

		self.states = list(
			chunks(sorted(list(self.STATES_MAP.keys())), 5)
		)
		
		self.states_label = Label(self.page2, text="States:")
		self.states_label.grid(row=1, column=0, sticky=W)
		self.states_grids = []
		row = 1
		for states in self.states:
			row += 2
			t = Checkbar(self.page2, states)
			self.states_grids.append(t)
			t.grid(row=row, column=0, sticky=N+S+E+W)

		PROPERTY_SUB_TYPE_MAP = {
			'airplane-hangar': 36,
			'airport':35,
			'amusement-park': 85,
			'apartments':58,
			'assisted-living':26,
			'auto-dealership':4,
			'auto-repair':5,
			'auto-salvage-facility':37,
			'bank':6,
			'bar':7,
			'baseball-field':86,
			'bowling-alley':87,
			'car-wash':8,
			'casino':88,
			'gravel-plant':38,
			'cemetery':72,
			'oil-refinery':39,
			'congregate-senior-housing':27,
			'continuing-care-retirement-community':28,
			'contractor-storage-yard':117,
			'convenience-store':9,
			'correctional-facility':73,
			'day-care-center':10,
			'department-store':116,
			'distribution':41,
			'dormitory':143,
			'drive-in-movie':115,
			'drug-store':11,
			'fast-food':12,
			'flex-showroom':3,
			'data-hosting':99,
			'food-processing':42,
			'freestanding':14,
			'funeral-home':13,
			'garden-center':15,
			'golf-course':89,
			'health-club':90,
			'horse-stables':91,
			'hospital':29,
			'hotel':32,
			'hotel-casino':33,
			'industrial-live':114,
			'industrial-showroom':49,
			'land-Agricultural':4,
			'land-commercial':2,
			'land-industrial':3,
			'land-residential':1,
			'landfill':43,
			'light-distribution':104,
			'light-manufacturing':1,
		}

		self.property_sub_types = list(
			chunks(sorted(list(PROPERTY_SUB_TYPE_MAP.keys())), 4)
		)
		
		# page 3
		self.property_sub_type_label = Label(self.page3, text="Secondary Type:")
		self.property_sub_type_label.grid(row=row, column=0, sticky=W)
		self.property_sub_type_grids = []
		for property_sub_type in self.property_sub_types:
			row += 2
			t = Checkbar(self.page3, property_sub_type)
			self.property_sub_type_grids.append(t)
			t.grid(row=row, column=0, sticky=N+S+E+W)




		self.reset_button = Button(self.page1, text="Save and Run", command=lambda: self.save())
		self.reset_button.grid(row=10, column=2, sticky=W+E)


	def save(self):
		
		config = configparser.ConfigParser()
		config.optionxform = str 
		states = ''
		for st in self.states_grids:
			states += ",".join(st.checked_names())

		property_sub_type = ''
		for st in self.property_sub_type_grids:
			property_sub_type += ",".join(st.checked_names())
		config['DEFAULT'] = {
			'MIN_PRICE': self.min_price or '',
			'MAX_PRICE': self.max_price or '',
			'REQUEST_DELAY': 1,
			'SALE_DATE_START': self.sale_date_start or '',
			'SALE_DATE_END': self.sale_date_end or '',
			'TENANCIES': ",".join(self.tenancies.checked_names()),
			'PROPERTY_TYPES': ",".join(self.property_types.checked_names()),
			'SALE_STATUSES': ",".join(self.sale_status.checked_names()),
			'OTHER_OPTIONS': ",".join(self.other_options.checked_names()),
			'STATES': states,
			'PROPERTY_SUB_TYPES':property_sub_type,
			'NON_DISCLOSED_PRICE': bool('include-non-disclosed-price' in self.non_disclosed_price_checkbox.checked_names())
		}
		with open('CONFIG.INI', 'w') as configfile:
			config.write(configfile)
		messagebox.showinfo("Costar v1", "Config file successfully generated, press Ok to Run ")
		main()


	def validate_min_price(self, new_text):
		if not new_text: # the field is being cleared
			self.min_price = 0
			return True

		try:
			self.min_price = int(new_text)
			return True
		except ValueError:
			return False

	def validate_max_price(self, new_text):
		if not new_text: # the field is being cleared
			self.max_price = 0
			return True

		try:
			self.max_price = int(new_text)
			return True
		except ValueError:
			return False

	def validate_sale_date_start(self, new_text):
		if not new_text: # the field is being cleared
			self.sale_date_start = ""
			return True

		try:
			self.sale_date_start = new_text
			return True
		except ValueError:
			return False



	def validate_sale_date_end(self, new_text):
		if not new_text: # the field is being cleared
			self.sale_date_end = ""
			return True

		try:
			self.sale_date_end = new_text
			return True
		except ValueError:
			return False

root = Tk()
my_gui = CostarGui(root)
root.mainloop()
