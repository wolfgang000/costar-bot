import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

driver = webdriver.Chrome('/home/wolfgang/drivers/chromedriver')  # Optional argument, if not specified will search path.
driver.get('https://gateway.costar.com/login'); 

username = driver.find_element_by_id("username")
password = driver.find_element_by_id("password")

username.send_keys("YourUsername")
password.send_keys("Pa55worD")


submit_button = WebDriverWait(driver, 25).until(
	expected_conditions.visibility_of_element_located((By.ID, "loginButton"))
)
submit_button.click()




time.sleep(5) 
driver.quit()