import requests
from lxml import html
import configparser
import os
import time


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

from marshmallow import Schema, fields, pprint

# Sales Search
####
class SearchResultSaleCompItemSchema(Schema):
	EntityId = fields.Integer()

class SearchResultItemSchema(Schema):
	SaleCompItems = fields.Nested(SearchResultSaleCompItemSchema, many=True)

class SearchResultSchema(Schema):
	Items = fields.Nested(SearchResultItemSchema, many=True)

SearchResultSerializer = SearchResultSchema()
#######

# Sale
####

class ContactsItemSchema(Schema):
	EmailAddress = fields.Str()
	Name = fields.Str()

class ContactsSchema(Schema):
	Items = fields.Nested(ContactsItemSchema, many=True)


class TrueBuyersItemSchema(Schema):
	Contacts = fields.Nested(ContactsSchema)

class TrueBuyersSchema(Schema):
	Items = fields.Nested(TrueBuyersItemSchema, many=True)

class SaleSchema(Schema):
	TrueBuyers = fields.Nested(TrueBuyersSchema)
	BuyerBrokers = fields.Nested(TrueBuyersSchema)
	TrueSellers = fields.Nested(TrueBuyersSchema)
	#TrueBuyers = fields.Dict()

SaleSerializer = SaleSchema()

####





import json
import datetime
import csv
import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

class CostarApi():
	SEARCH_URL = "http://property.costar.com/Exchange/ForSaleSearch/SearchPage/Index/US"
	SALES_SEARCH_URL = "http://property.costar.com/pds/saleComp/search/"
	PROPERTY_TYPE_MAP = {
		'hospitality': 1,
		'industrial': 2,
		'land': 3,
		'office':5,
		'retail':6,
		'flex': 7,
		'sports-entertainment': 8,
		'specialty': 10,
		'multi-family': 11,
		'health-care': 13,
		'shopping-center': 999,
	}
	PROPERTY_SUB_TYPE_MAP = {
		'airplane-hangar': 36,
		'airport':35,
		'amusement-park': 85,
		'apartments':58,
		'assisted-living':26,
		'auto-dealership':4,
		'auto-repair':5,
		'auto-salvage-facility':37,
		'bank':6,
		'bar':7,
		'baseball-field':86,
		'bowling-alley':87,
		'car-wash':8,
		'casino':88,
		'gravel-plant':38,
		'cemetery':72,
		'oil-refinery':39,
		'congregate-senior-housing':27,
		'continuing-care-retirement-community':28,
		'contractor-storage-yard':117,
		'convenience-store':9,
		'correctional-facility':73,
		'day-care-center':10,
		'department-store':116,
		'distribution':41,
		'dormitory':143,
		'drive-in-movie':115,
		'drug-store':11,
		'fast-food':12,
		'flex-showroom':3,
		'data-hosting':99,
		'food-processing':42,
		'freestanding':14,
		'funeral-home':13,
		'garden-center':15,
		'golf-course':89,
		'health-club':90,
		'horse-stables':91,
		'hospital':29,
		'hotel':32,
		'hotel-casino':33,
		'industrial-live':114,
		'industrial-showroom':49,
		'land-Agricultural':4,
		'land-commercial':2,
		'land-industrial':3,
		'land-residential':1,
		'landfill':43,
		'light-distribution':104,
		'light-manufacturing':1,
	}
	TENANCY_MAP = {
		'single': 1, 
		'multiple': 2
	}
	SALE_STATUS_MAP = {
		'sold': 5, 
		'for-sale': 1,
		'under-contract': 4,
	}

	def __init__(self, delay_time = 5):
		self.delay_time = 5
		self.session_requests = requests.session()
		cookies_path = os.path.join(BASE_DIR, 'cookies.json')
		with open(cookies_path, 'r') as file:
			cookies_json = file.read()
		cookies = json.loads(cookies_json)
	
		states_path = os.path.join(BASE_DIR, os.path.join('resources','states.json'))
		with open(states_path, 'r') as file:
			states_json = file.read()
		self.STATES_MAP = json.loads(states_json)

		self.session_requests.cookies = requests.cookies.cookiejar_from_dict(cookies)
		
		# This request set the rest of the cookies necessaries
		response = self.session_requests.get(
			"http://property.costar.com/Exchange/ForSaleSearch/SearchPage/Index/US", 
			headers = dict(referer =  "http://gateway.costar.com/Home/"), 
			timeout = 20
		)
		time.sleep(self.delay_time)

	
	def sales_search(self, payload):
		headers = {
			"referer": "http://property.costar.com/Exchange/ForSaleSearch/SearchPage/Index/US",
			'Content-Type': 'application/json', 
			'Accept': 'application/json'
		}
		response = self.session_requests.post(
			"http://property.costar.com/pds/saleComp/search/", 
			headers = headers, 
			json=payload,
			timeout = 20
		)
		time.sleep(self.delay_time)
		if response.status_code >= 401:
			raise self.SessionExpired()
		search_results, errors = SearchResultSerializer.loads(response.content.decode('utf_8'))
		return search_results
	
	def get_sales_data(self, sale_id):
		headers = {
			"referer": "http://property.costar.com/Exchange/ForSaleSearch/SearchPage/Index/US", 
			'Accept': 'application/json'
		}
		try:
			response = self.session_requests.get(
				"http://property.costar.com/pds/saleComp/{}".format(sale_id), 
				headers = headers, 
				timeout = 20
			)
			time.sleep(self.delay_time)
			if response.status_code == 200:
				sale, errors = SaleSerializer.loads(response.content.decode('utf_8'))
				return sale
			elif response.status_code == 401:
				raise self.SessionExpired()

			print("Error in request with id {} with status {}, check the id in http://property.costar.com/Comps/Lookup/LookupForm.aspx".format(sale_id,response.status_code))
			return {}
		except Exception:
			print("Timeout")
			return {}
	
	def build_search_payload_form_config_dict(self, config):
		payload = {
			"Property": {},
			"Sale": {
				"SaleComp":{
					"ReturnPortfoliosAsIndividuals":False,
					"ExcludeNonArmsLength": True,
					"Source": 0,
				},
				
			},
			"Geography": {
				"CountryCode": "US"
			}
		}
		property_types = config.get('PROPERTY_TYPES')
		if property_types:
			payload['Property']['PropertyTypes'] = []
			property_types = property_types.split(',')
			for property_type in property_types:
				property_type_index = self.PROPERTY_TYPE_MAP.get(property_type)
				if property_type_index:
					payload['Property']['PropertyTypes'].append(property_type_index)
	

		property_sub_types = config.get('PROPERTY_SUB_TYPES')
		if property_sub_types:
			payload['Property']['Building'] = {"PropertySubtypes": [] }
			property_sub_types = property_sub_types.split(',')
			for property_type in property_sub_types:
				property_type_index = self.PROPERTY_SUB_TYPE_MAP.get(property_type)
				if property_type_index:
					payload['Property']['Building']['PropertySubtypes'].append(property_type_index)

		tenancies = config.get('TENANCIES')
		if tenancies:
			if not payload['Property'].get('Building'):
				payload['Property']['Building'] = {'Tenancy': []}
			else:
				payload['Property']['Building']['Tenancy'] = []
			
			tenancies = tenancies.split(',')
			for tenancy in tenancies:
				tenancy_index = self.TENANCY_MAP.get(tenancy)
				if tenancy_index:
					payload['Property']['Building']['Tenancy'].append(tenancy_index)


		min_price = config.get('MIN_PRICE')
		max_price = config.get('MAX_PRICE')
		nan_discloed_price_field = config.get('NON_DISCLOSED_PRICE', 'False')

		if nan_discloed_price_field == 'True':
			nan_discloed_price = True
		else:
			nan_discloed_price = False

		if min_price and max_price:
			price = {
				"TotalPrice": {
					"Minimum": {
						"Value": int(min_price),
						"Code": "USD"
					},
					"Maximum": {
						"Value": int(max_price),
						"Code": "USD"
					}
				},
				"IncludeNonDisclosedPrice": nan_discloed_price
			}
			payload['Sale']['SaleComp']['Price'] = price
		
		sale_date_start = config.get('SALE_DATE_START')
		sale_date_end = config.get('SALE_DATE_END')
		if sale_date_start and sale_date_end:
			sale_date_start = datetime.datetime.strptime(sale_date_start, "%m/%d/%Y")
			sale_date_end = datetime.datetime.strptime(sale_date_end, "%m/%d/%Y")
			sold_date = {
				"Minimum": str(sale_date_start),
				"Maximum": str(sale_date_end)
			}
			payload['Sale']['SaleComp']['SoldDate'] = sold_date
		
		sale_statuses = config.get('SALE_STATUSES')
		if sale_statuses :
			payload['Sale']['SaleComp']["SaleStatuses"] =[]

			
			sale_statuses = sale_statuses.split(',')
			for sale_status in sale_statuses:
				sale_status_dict = self.SALE_STATUS_MAP.get(sale_status)
				if sale_status_dict:
					payload['Sale']['SaleComp']["SaleStatuses"].append(sale_status_dict)

		states = config.get('STATES')
		if states:
			payload['Geography']["Filter"] = {
				"Ids": [],
				"FilterType": 11,
				"AdditionalData": [],
			}
			states = states.split(',')
			for state in states:
				state_dict = self.STATES_MAP.get(state)
				if state_dict:
					payload['Geography']["Filter"]["Ids"].append(state_dict.get("subdivisionId"))
					payload['Geography']["Filter"]["AdditionalData"].append(state_dict)
		
		other_options = config.get('OTHER_OPTIONS')
		if other_options:
			for other_option in other_options:
				if other_option == 'include-non-arms-length-comps':
					payload["Sale"]["SaleComp"]["ExcludeNonArmsLength"] = False
				if other_option == 'include-nnn-sales-only':
					payload["Sale"]["SaleComp"]["IncludeTripleNetSales"] = True
				if other_option == 'include-public-record-sales':
					payload["Sale"]["SaleComp"].pop("Source")
				if other_option == 'return-portfolios-as-individuals':
					payload["Sale"]["Source"]["ReturnPortfoliosAsIndividuals"] = True

		payload["BoundingBox"] = { 
			"LowerRight": { 
			"Latitude":  "-46.46782579103605", 
			"Longitude": "-30.371590193749967", 
			}, 
			"UpperLeft": { 
			"Latitude":  "73.31537383954927", 
			"Longitude":"-134.25830894374997", 
			}, 
		} 

		return payload


	def get_sales_data_and_write_to_csv(self, search_results):
		with open(os.path.join(BASE_DIR, os.path.join('files','search-{}.csv'.format(datetime.datetime.now()))),'w') as new_file:
			newFileWriter = csv.writer(new_file)
			newFileWriter.writerow(['sale_id','true_buyer','true_buyer_email','buyer','buyer_email','seller','seller_email',])

			
			for sales_items in search_results['Items']:
				for sale_comp_items in sales_items["SaleCompItems"]:
					sale_id = sale_comp_items["EntityId"]
					if sale_id:
						sale = self.get_sales_data(sale_id)
						true_buyer_contacts = {}
						true_seller_contacts = {}
						buyer_broker_contacts = {}
						try:
							true_buyer = sale.get("TrueBuyers")
							true_buyer_contacts = true_buyer['Items'][0]['Contacts']['Items'][0]
						except Exception:
							pass
						
						try:
							true_seller = sale.get("TrueSellers")
							true_seller_contacts = true_seller['Items'][0]['Contacts']['Items'][0]
						except Exception:
							pass
						
						try:
							buyer_broker = sale.get("BuyerBrokers")
							buyer_broker_contacts = buyer_broker['Items'][0]['Contacts']['Items'][0]
						except Exception:
							pass

						if true_buyer_contacts.get("EmailAddress") or buyer_broker_contacts.get("EmailAddress") or true_seller_contacts.get("EmailAddress"):
							print("Writing sale:{} emails".format(sale_id))
							sale_data = [
								sale_id,
								true_buyer_contacts.get("Name"),
								true_buyer_contacts.get("EmailAddress"),
								buyer_broker_contacts.get("Name"),
								buyer_broker_contacts.get("EmailAddress"),
								true_seller_contacts.get("Name"),
								true_seller_contacts.get("EmailAddress"),
							]
							print(sale_data)
							newFileWriter.writerow(sale_data)
						else:
							print("No emails were found in sale:{} check the id in http://property.costar.com/Comps/Lookup/LookupForm.aspx".format(sale_id))
		print("#######################################################")
		print("Search Finish")
		print("#######################################################")
	
	class SessionExpired(Exception):
		pass


from login import main as login_main

def main():
	config = configparser.ConfigParser()
	config.read('CONFIG.INI')
	default = config['DEFAULT']
	request_delay = int(default['REQUEST_DELAY'])

	bot = CostarApi(delay_time=request_delay)
	payload = bot.build_search_payload_form_config_dict(default)
	print("To validate the results check the ids in http://property.costar.com/Comps/Lookup/LookupForm.aspx")
	try:
		print(payload)
		sales = bot.sales_search(payload)
		print("quantity:",len(sales['Items']))
		bot.get_sales_data_and_write_to_csv(sales)
	except CostarApi.SessionExpired:
		print("Session expired, Performing Login again....")
		login_main()
		bot = CostarApi(delay_time=request_delay)
		sales = bot.sales_search(payload)
		print("quantity:",len(sales['Items']))
		bot.get_sales_data_and_write_to_csv(sales)



if __name__ == '__main__':
    main()
